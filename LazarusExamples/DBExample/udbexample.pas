{
Powerpdf DBExample ported manually to Lazarus.
Source file is copied from original delphi example.
cvs-file is taken from lazarusexamples\ConsoleProgramDemo
}
unit UDBExample;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, csvdataset, DB, Forms, Controls, Graphics, Dialogs, Menus, PReport;

type

  { TForm1 }

  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    miFile: TMenuItem;
    miHelp: TMenuItem;
    miCreatePDF: TMenuItem;
    miExit: TMenuItem;
    miAbout: TMenuItem;
    N1: TMenuItem;
    PReport1: TPReport;
    PRGridPanel1: TPRGridPanel;
    PRLayoutPanel1: TPRLayoutPanel;
    PRLayoutPanel2: TPRLayoutPanel;
    PRPage1: TPRPage;
    PRRect1: TPRRect;
    PRRect2: TPRRect;
    PRText1: TPRText;
    PRText2: TPRText;
    PRText3: TPRText;
    PRText4: TPRText;
    PRText5: TPRText;
    PRText6: TPRText;
    SaveDialog1: TSaveDialog;
    ScrollBox1: TScrollBox;
    Table1: TCSVDataset;
    TxtAddr: TPRText;
    TxtCity: TPRText;
    TxtCompany: TPRText;
    TxtCustNo: TPRText;
    TxtState: TPRText;
    procedure FormCreate(Sender: TObject);
    procedure miAboutClick(Sender: TObject);
    procedure miCreatePDFClick(Sender: TObject);
    procedure miExitClick(Sender: TObject);
    procedure PRGridPanel1BeforePrintChild(Sender: TObject; ACanvas: TPRCanvas; ACol,
      ARow: integer; Rect: TRect);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  Table1.LoadFromFile(ExtractFilePath(Application.ExeName)+'customer.csv');

  PRPage1.Visible := false;
end;

procedure TForm1.miAboutClick(Sender: TObject);
begin
  ShowMessage(POWER_PDF_VERSION_STR + #13#10 + POWER_PDF_COPYRIGHT);
end;

procedure TForm1.miExitClick(Sender: TObject);
begin
  Close;
end;

procedure TForm1.miCreatePDFClick(Sender: TObject);
begin
  if SaveDialog1.Execute then
  begin
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    TxtCustNo.Printable := true;
    TxtCompany.Printable := true;
    TxtAddr.Printable := true;
    TxtCity.Printable := true;
    TxtState.Printable := true;
    try
      with PReport1 do
      begin
        FileName := SaveDialog1.FileName;

        // starting printing document.
        BeginDoc;

        Table1.Open;
        while not Table1.Eof do
          Print(PRPage1);

        // save document.
        EndDoc;
        Table1.Close;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TForm1.PRGridPanel1BeforePrintChild(Sender: TObject; ACanvas: TPRCanvas; ACol,
  ARow: integer; Rect: TRect);
begin
  with Table1 do
    if not Table1.Eof then
    begin
      // setting text from current record.
      TxtCustNo.Text := Table1.Fields[0].AsString;
      TxtCompany.Text := Table1.Fields[1].AsString;
      TxtAddr.Text := Table1.Fields[4].AsString;
      TxtCity.Text := Table1.Fields[3].AsString;
      TxtState.Text := Table1.Fields[2].AsString;

      // move next current record.
      Table1.Next;
    end
    else
    begin
      TxtCustNo.Printable := false;
      TxtCompany.Printable := false;
      TxtAddr.Printable := false;
      TxtCity.Printable := false;
      TxtState.Printable := false;
    end;
end;


end.

